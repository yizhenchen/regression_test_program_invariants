===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionAsFloat():::EXIT1117
===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionAsFloat():::EXIT
return == 1.8
===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionAsInt():::EXIT1145
===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionAsInt():::EXIT
return == 180
===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionMatches(java.lang.String):::ENTER
===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionMatches(java.lang.String):::EXIT1175
===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionMatches(java.lang.String):::EXIT1175;condition="not(return == true)"
===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionMatches(java.lang.String):::EXIT
return == false
versionPrefix.toString == orig(versionPrefix.toString)
===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionMatches(java.lang.String):::EXIT;condition="not(return == true)"
===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionTrimmed():::EXIT1158
===========================================================================
org.apache.commons.lang.SystemUtils.getJavaVersionTrimmed():::EXIT
return has only one value
return.toString == "1.8.0_45"
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String):::ENTER
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String):::EXIT1188
(return == true)  ==>  (orig(osNamePrefix) has only one value)
(return == true)  ==>  (osNamePrefix.toString == "Linux")
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String):::EXIT1188;condition="return == true"
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String):::EXIT1188;condition="not(return == true)"
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String):::EXIT
(return == true)  ==>  (orig(osNamePrefix) has only one value)
(return == true)  ==>  (osNamePrefix.toString == "Linux")
osNamePrefix.toString == orig(osNamePrefix.toString)
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String):::EXIT;condition="return == true"
osNamePrefix.toString == "Linux"
return == true
orig(osNamePrefix) has only one value
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String):::EXIT;condition="not(return == true)"
return == false
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String, java.lang.String):::ENTER
osNamePrefix.toString one of { "Windows", "Windows 9" }
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String, java.lang.String):::EXIT1203
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String, java.lang.String):::EXIT1203;condition="not(return == true)"
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String, java.lang.String):::EXIT
osNamePrefix.toString one of { "Windows", "Windows 9" }
return == false
osNamePrefix.toString == orig(osNamePrefix.toString)
osVersionPrefix.toString == orig(osVersionPrefix.toString)
===========================================================================
org.apache.commons.lang.SystemUtils.getOSMatches(java.lang.String, java.lang.String):::EXIT;condition="not(return == true)"
===========================================================================
org.apache.commons.lang.SystemUtils.getSystemProperty(java.lang.String):::ENTER
property != null
===========================================================================
org.apache.commons.lang.SystemUtils.getSystemProperty(java.lang.String):::EXIT1218
===========================================================================
org.apache.commons.lang.SystemUtils.getSystemProperty(java.lang.String):::EXIT
property.toString != return.toString
property.toString == orig(property.toString)
===========================================================================
org.apache.commons.lang.WordUtils:::OBJECT
this has only one value
===========================================================================
org.apache.commons.lang.WordUtils.WordUtils():::EXIT
===========================================================================
org.apache.commons.lang.WordUtils.capitalize(java.lang.String):::ENTER
===========================================================================
org.apache.commons.lang.WordUtils.capitalize(java.lang.String):::EXIT
str.toString >= return.toString
str.toString == orig(str.toString)
===========================================================================
org.apache.commons.lang.WordUtils.capitalize(java.lang.String, char[]):::ENTER
delimiters[] one of { [], [45, 43, 32, 64], [46] }
size(delimiters[]) one of { 0, 1, 4 }
delimiters[] elements > size(delimiters[])
===========================================================================
org.apache.commons.lang.WordUtils.capitalize(java.lang.String, char[]):::EXIT276
return == orig(str)
str.toString == ""
delimiters[] == []
str.toString == return.toString
===========================================================================
org.apache.commons.lang.WordUtils.capitalize(java.lang.String, char[]):::EXIT312
return != null
orig(str) != null
===========================================================================
org.apache.commons.lang.WordUtils.capitalize(java.lang.String, char[]):::EXIT
delimiters[] == orig(delimiters[])
delimiters[] one of { [], [45, 43, 32, 64], [46] }
str.toString >= return.toString
str.toString == orig(str.toString)
delimiters[] elements > orig(size(delimiters[]))
===========================================================================
org.apache.commons.lang.WordUtils.capitalizeFully(java.lang.String):::ENTER
===========================================================================
org.apache.commons.lang.WordUtils.capitalizeFully(java.lang.String):::EXIT
str.toString == orig(str.toString)
===========================================================================
org.apache.commons.lang.WordUtils.capitalizeFully(java.lang.String, char[]):::ENTER
delimiters[] one of { [], [45, 43, 32, 64], [46] }
size(delimiters[]) one of { 0, 1, 4 }
delimiters[] elements > size(delimiters[])
===========================================================================
org.apache.commons.lang.WordUtils.capitalizeFully(java.lang.String, char[]):::EXIT366
return == orig(str)
str.toString == ""
delimiters[] == []
str.toString == return.toString
str.toString == orig(str.toString)
===========================================================================
org.apache.commons.lang.WordUtils.capitalizeFully(java.lang.String, char[]):::EXIT369
return != null
orig(str) != null
===========================================================================
org.apache.commons.lang.WordUtils.capitalizeFully(java.lang.String, char[]):::EXIT
delimiters[] == orig(delimiters[])
delimiters[] one of { [], [45, 43, 32, 64], [46] }
delimiters[] elements > orig(size(delimiters[]))
===========================================================================
org.apache.commons.lang.WordUtils.swapCase(java.lang.String):::ENTER
===========================================================================
org.apache.commons.lang.WordUtils.swapCase(java.lang.String):::EXIT484
return == orig(str)
str.toString == ""
str.toString == return.toString
===========================================================================
org.apache.commons.lang.WordUtils.swapCase(java.lang.String):::EXIT510
===========================================================================
org.apache.commons.lang.WordUtils.swapCase(java.lang.String):::EXIT
str.toString == orig(str.toString)
===========================================================================
org.apache.commons.lang.WordUtils.uncapitalize(java.lang.String):::ENTER
===========================================================================
org.apache.commons.lang.WordUtils.uncapitalize(java.lang.String):::EXIT
str.toString <= return.toString
str.toString == orig(str.toString)
===========================================================================
org.apache.commons.lang.WordUtils.uncapitalize(java.lang.String, char[]):::ENTER
delimiters[] one of { [], [45, 43, 32, 64], [46] }
size(delimiters[]) one of { 0, 1, 4 }
delimiters[] elements > size(delimiters[])
===========================================================================
org.apache.commons.lang.WordUtils.uncapitalize(java.lang.String, char[]):::EXIT420
return == orig(str)
str.toString == ""
delimiters[] == []
str.toString == return.toString
===========================================================================
org.apache.commons.lang.WordUtils.uncapitalize(java.lang.String, char[]):::EXIT456
return != null
orig(str) != null
===========================================================================
org.apache.commons.lang.WordUtils.uncapitalize(java.lang.String, char[]):::EXIT
delimiters[] == orig(delimiters[])
delimiters[] one of { [], [45, 43, 32, 64], [46] }
str.toString <= return.toString
str.toString == orig(str.toString)
delimiters[] elements > orig(size(delimiters[]))
===========================================================================
org.apache.commons.lang.WordUtils.wrap(java.lang.String, int):::ENTER
wrapLength one of { -1, 20 }
===========================================================================
org.apache.commons.lang.WordUtils.wrap(java.lang.String, int):::EXIT
str.toString == orig(str.toString)
===========================================================================
org.apache.commons.lang.WordUtils.wrap(java.lang.String, int, java.lang.String, boolean):::ENTER
newLineStr.toString one of { "\n", "<br />" }
str.toString != newLineStr.toString
===========================================================================
org.apache.commons.lang.WordUtils.wrap(java.lang.String, int, java.lang.String, boolean):::EXIT164
return == orig(str)
newLineStr.toString == "\n"
return == null
orig(wrapLength) one of { -1, 20 }
===========================================================================
org.apache.commons.lang.WordUtils.wrap(java.lang.String, int, java.lang.String, boolean):::EXIT214
return != null
orig(str) != null
===========================================================================
org.apache.commons.lang.WordUtils.wrap(java.lang.String, int, java.lang.String, boolean):::EXIT
(return != null)  <==>  (orig(str) != null)
(return != null)  ==>  (newLineStr != null)
(return != null)  ==>  (newLineStr.toString != return.toString)
(return != null)  ==>  (newLineStr.toString one of { "\n", "<br />" })
(return != null)  ==>  (orig(newLineStr.toString) one of { "\n", "<br />" })
(return != null)  ==>  (return.toString != orig(newLineStr.toString))
(return != null)  ==>  (str.toString != newLineStr.toString)
(return != null)  ==>  (str.toString != orig(newLineStr.toString))
(return == null)  <==>  (orig(str) == null)
(return == null)  ==>  (newLineStr == orig(newLineStr))
(return == null)  ==>  (newLineStr.toString == "\n")
(return == null)  ==>  (orig(newLineStr.toString) == "\n")
(return == null)  ==>  (orig(wrapLength) one of { -1, 20 })
(return == null)  ==>  (return == orig(str))
(return == null)  ==>  (wrapLength == orig(wrapLength))
(return == null)  ==>  (wrapLength one of { -1, 20 })
str.toString == orig(str.toString)
str.toString != orig(newLineStr.toString)
return.toString != orig(newLineStr.toString)
===========================================================================
org.apache.commons.lang.WordUtilsTest:::CLASS
org.apache.commons.lang.WordUtilsTest.class$0 == null
===========================================================================
org.apache.commons.lang.WordUtilsTest:::OBJECT
===========================================================================
org.apache.commons.lang.WordUtilsTest.WordUtilsTest(java.lang.String):::ENTER
===========================================================================
org.apache.commons.lang.WordUtilsTest.WordUtilsTest(java.lang.String):::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
name.toString == orig(name.toString)
===========================================================================
org.apache.commons.lang.WordUtilsTest.testCapitalizeFullyWithDelimiters_String():::ENTER
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
this has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testCapitalizeFullyWithDelimiters_String():::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$0)
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$1)
===========================================================================
org.apache.commons.lang.WordUtilsTest.testCapitalizeFully_String():::ENTER
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
this has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testCapitalizeFully_String():::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$0)
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$1)
===========================================================================
org.apache.commons.lang.WordUtilsTest.testCapitalizeWithDelimiters_String():::ENTER
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
this has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testCapitalizeWithDelimiters_String():::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$0)
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$1)
===========================================================================
org.apache.commons.lang.WordUtilsTest.testCapitalize_String():::ENTER
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
this has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testCapitalize_String():::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$0)
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$1)
===========================================================================
org.apache.commons.lang.WordUtilsTest.testConstructor():::ENTER
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
this has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testConstructor():::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$0)
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$1)
org.apache.commons.lang.WordUtilsTest.class$1 has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testSwapCase_String():::ENTER
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
this has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testSwapCase_String():::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$0)
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$1)
===========================================================================
org.apache.commons.lang.WordUtilsTest.testUncapitalizeWithDelimiters_String():::ENTER
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
this has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testUncapitalizeWithDelimiters_String():::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$0)
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$1)
===========================================================================
org.apache.commons.lang.WordUtilsTest.testUncapitalize_String():::ENTER
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
this has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testUncapitalize_String():::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$0)
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$1)
===========================================================================
org.apache.commons.lang.WordUtilsTest.testWrap_StringInt():::ENTER
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
this has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testWrap_StringInt():::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$0)
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$1)
===========================================================================
org.apache.commons.lang.WordUtilsTest.testWrap_StringIntStringBoolean():::ENTER
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
this has only one value
===========================================================================
org.apache.commons.lang.WordUtilsTest.testWrap_StringIntStringBoolean():::EXIT
org.apache.commons.lang.WordUtilsTest.class$0 == org.apache.commons.lang.WordUtilsTest.class$1
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$0)
org.apache.commons.lang.WordUtilsTest.class$0 == orig(org.apache.commons.lang.WordUtilsTest.class$1)
===========================================================================
org.apache.commons.lang.daikon.WordUtilsTest:::CLASS
===========================================================================
org.apache.commons.lang.daikon.WordUtilsTest.main(java.lang.String[]):::ENTER
org.apache.commons.lang.daikon.WordUtilsTest.class$0 == null
arg has only one value
arg.getClass().getName() == java.lang.String[].class
arg[] == []
arg[].toString == []
===========================================================================
org.apache.commons.lang.daikon.WordUtilsTest.main(java.lang.String[]):::EXIT
arg[] == orig(arg[])
org.apache.commons.lang.daikon.WordUtilsTest.class$0 has only one value
arg[] == []
arg[].toString == []
