===========================================================================
org.apache.commons.lang.daikon.MutableFloatTest:::CLASS
===========================================================================
org.apache.commons.lang.daikon.MutableFloatTest.main(java.lang.String[]):::ENTER
org.apache.commons.lang.daikon.MutableFloatTest.class$0 == null
arg has only one value
arg.getClass().getName() == java.lang.String[].class
arg[] == []
arg[].toString == []
===========================================================================
org.apache.commons.lang.daikon.MutableFloatTest.main(java.lang.String[]):::EXIT
arg[] == orig(arg[])
org.apache.commons.lang.daikon.MutableFloatTest.class$0 has only one value
arg[] == []
arg[].toString == []
===========================================================================
org.apache.commons.lang.math.NumberUtils:::CLASS
org.apache.commons.lang.math.NumberUtils.LONG_ZERO has only one value
org.apache.commons.lang.math.NumberUtils.LONG_ONE has only one value
org.apache.commons.lang.math.NumberUtils.LONG_MINUS_ONE has only one value
org.apache.commons.lang.math.NumberUtils.INTEGER_ZERO has only one value
org.apache.commons.lang.math.NumberUtils.INTEGER_ONE has only one value
org.apache.commons.lang.math.NumberUtils.INTEGER_MINUS_ONE has only one value
org.apache.commons.lang.math.NumberUtils.SHORT_ZERO has only one value
org.apache.commons.lang.math.NumberUtils.SHORT_ONE has only one value
org.apache.commons.lang.math.NumberUtils.SHORT_MINUS_ONE has only one value
org.apache.commons.lang.math.NumberUtils.BYTE_ZERO has only one value
org.apache.commons.lang.math.NumberUtils.BYTE_ONE has only one value
org.apache.commons.lang.math.NumberUtils.BYTE_MINUS_ONE has only one value
org.apache.commons.lang.math.NumberUtils.DOUBLE_ZERO has only one value
org.apache.commons.lang.math.NumberUtils.DOUBLE_ONE has only one value
org.apache.commons.lang.math.NumberUtils.DOUBLE_MINUS_ONE has only one value
org.apache.commons.lang.math.NumberUtils.FLOAT_ZERO has only one value
org.apache.commons.lang.math.NumberUtils.FLOAT_ONE has only one value
org.apache.commons.lang.math.NumberUtils.FLOAT_MINUS_ONE has only one value
===========================================================================
org.apache.commons.lang.math.NumberUtils.compare(float, float):::ENTER
lhs == 0.0
rhs one of { -1.0, 0.0, 1.0 }
===========================================================================
org.apache.commons.lang.math.NumberUtils.compare(float, float):::EXIT1242
return == -1
orig(rhs) == 1.0
===========================================================================
org.apache.commons.lang.math.NumberUtils.compare(float, float):::EXIT1245
return == 1
orig(rhs) == -1.0
===========================================================================
org.apache.commons.lang.math.NumberUtils.compare(float, float):::EXIT1254
orig(lhs) == orig(rhs)
return == 0
===========================================================================
org.apache.commons.lang.math.NumberUtils.compare(float, float):::EXIT
org.apache.commons.lang.math.NumberUtils.LONG_ZERO == orig(org.apache.commons.lang.math.NumberUtils.LONG_ZERO)
org.apache.commons.lang.math.NumberUtils.LONG_ONE == orig(org.apache.commons.lang.math.NumberUtils.LONG_ONE)
org.apache.commons.lang.math.NumberUtils.LONG_MINUS_ONE == orig(org.apache.commons.lang.math.NumberUtils.LONG_MINUS_ONE)
org.apache.commons.lang.math.NumberUtils.INTEGER_ZERO == orig(org.apache.commons.lang.math.NumberUtils.INTEGER_ZERO)
org.apache.commons.lang.math.NumberUtils.INTEGER_ONE == orig(org.apache.commons.lang.math.NumberUtils.INTEGER_ONE)
org.apache.commons.lang.math.NumberUtils.INTEGER_MINUS_ONE == orig(org.apache.commons.lang.math.NumberUtils.INTEGER_MINUS_ONE)
org.apache.commons.lang.math.NumberUtils.SHORT_ZERO == orig(org.apache.commons.lang.math.NumberUtils.SHORT_ZERO)
org.apache.commons.lang.math.NumberUtils.SHORT_ONE == orig(org.apache.commons.lang.math.NumberUtils.SHORT_ONE)
org.apache.commons.lang.math.NumberUtils.SHORT_MINUS_ONE == orig(org.apache.commons.lang.math.NumberUtils.SHORT_MINUS_ONE)
org.apache.commons.lang.math.NumberUtils.BYTE_ZERO == orig(org.apache.commons.lang.math.NumberUtils.BYTE_ZERO)
org.apache.commons.lang.math.NumberUtils.BYTE_ONE == orig(org.apache.commons.lang.math.NumberUtils.BYTE_ONE)
org.apache.commons.lang.math.NumberUtils.BYTE_MINUS_ONE == orig(org.apache.commons.lang.math.NumberUtils.BYTE_MINUS_ONE)
org.apache.commons.lang.math.NumberUtils.DOUBLE_ZERO == orig(org.apache.commons.lang.math.NumberUtils.DOUBLE_ZERO)
org.apache.commons.lang.math.NumberUtils.DOUBLE_ONE == orig(org.apache.commons.lang.math.NumberUtils.DOUBLE_ONE)
org.apache.commons.lang.math.NumberUtils.DOUBLE_MINUS_ONE == orig(org.apache.commons.lang.math.NumberUtils.DOUBLE_MINUS_ONE)
org.apache.commons.lang.math.NumberUtils.FLOAT_ZERO == orig(org.apache.commons.lang.math.NumberUtils.FLOAT_ZERO)
org.apache.commons.lang.math.NumberUtils.FLOAT_ONE == orig(org.apache.commons.lang.math.NumberUtils.FLOAT_ONE)
org.apache.commons.lang.math.NumberUtils.FLOAT_MINUS_ONE == orig(org.apache.commons.lang.math.NumberUtils.FLOAT_MINUS_ONE)
return one of { -1, 0, 1 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat:::CLASS
===========================================================================
org.apache.commons.lang.mutable.MutableFloat:::OBJECT
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.MutableFloat():::EXIT
this.value == 0.0
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.MutableFloat(float):::ENTER
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.MutableFloat(float):::EXIT
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.MutableFloat(java.lang.Number):::ENTER
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.MutableFloat(java.lang.Number):::EXIT
this.value one of { 2.0, 3.0 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.compareTo(java.lang.Object):::ENTER
this has only one value
this.value == 0.0
obj.getClass().getName() == org.apache.commons.lang.mutable.MutableFloat.class
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.compareTo(java.lang.Object):::EXIT
this.value == orig(this.value)
this.value == 0.0
return one of { -1, 0, 1 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.doubleValue():::ENTER
this has only one value
this.value == 1.7
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.doubleValue():::EXIT
this.value == orig(this.value)
this.value == 1.7
return == 1.7000000476837158
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.equals(java.lang.Object):::ENTER
this.value one of { 0.0, 1.0 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.equals(java.lang.Object):::EXIT190
return == true
orig(obj.getClass().getName()) == org.apache.commons.lang.mutable.MutableFloat.class
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.equals(java.lang.Object):::EXIT190;condition="return == true"
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.equals(java.lang.Object):::EXIT191
this.value == 0.0
return == false
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.equals(java.lang.Object):::EXIT191;condition="not(return == true)"
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.equals(java.lang.Object):::EXIT
this.value == orig(this.value)
(return == false)  ==>  (this.value == 0.0)
(return == true)  ==>  (orig(obj.getClass().getName()) == org.apache.commons.lang.mutable.MutableFloat.class)
(return == true)  ==>  (this.value one of { 0.0, 1.0 })
this.value one of { 0.0, 1.0 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.equals(java.lang.Object):::EXIT;condition="return == true"
return == true
orig(obj.getClass().getName()) == org.apache.commons.lang.mutable.MutableFloat.class
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.equals(java.lang.Object):::EXIT;condition="not(return == true)"
this.value == 0.0
return == false
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.floatValue():::ENTER
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.floatValue():::EXIT
this.value == return
return == orig(this.value)
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.getValue():::ENTER
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.getValue():::EXIT
this.value == orig(this.value)
return.getClass().getName() == java.lang.Float.class
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.hashCode():::ENTER
this.value one of { 0.0, 1.0 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.hashCode():::EXIT
this.value == orig(this.value)
this.value one of { 0.0, 1.0 }
return one of { 0, 1065353216 }
org.apache.commons.lang.mutable.MutableFloat.serialVersionUID > return
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.intValue():::ENTER
this has only one value
this.value == 1.7
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.intValue():::EXIT
this.value == orig(this.value)
this.value == 1.7
return == 1
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.isInfinite():::ENTER
this.value one of { -Infinity, Infinity }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.isInfinite():::EXIT
this.value == orig(this.value)
this.value one of { -Infinity, Infinity }
return == true
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.isInfinite():::EXIT;condition="return == true"
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.isNaN():::ENTER
this has only one value
this.value == Double.NaN
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.isNaN():::EXIT
this.value == Double.NaN
return == true
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.isNaN():::EXIT;condition="return == true"
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.longValue():::ENTER
this has only one value
this.value == 1.7
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.longValue():::EXIT
this.value == orig(this.value)
this.value == 1.7
return == 1
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.setValue(float):::ENTER
this has only one value
this.value one of { 0.0, 1.0, 2.0 }
value one of { 1.0, 2.0, 3.0 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.setValue(float):::EXIT
this.value == orig(value)
this.value one of { 1.0, 2.0, 3.0 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.setValue(java.lang.Object):::ENTER
this has only one value
this.value one of { 1.0, 2.0 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.setValue(java.lang.Object):::EXIT
this.value one of { 2.0, 3.0 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.toString():::ENTER
this.value one of { -123.0, 0.0, 10.0 }
===========================================================================
org.apache.commons.lang.mutable.MutableFloat.toString():::EXIT
this.value == orig(this.value)
this.value one of { -123.0, 0.0, 10.0 }
return.toString one of { "-123.0", "0.0", "10.0" }
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest:::CLASS
org.apache.commons.lang.mutable.MutableFloatTest.class$0 == null
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest:::OBJECT
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.MutableFloatTest(java.lang.String):::ENTER
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.MutableFloatTest(java.lang.String):::EXIT
testName.toString == orig(testName.toString)
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testCompareTo():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testCompareTo():::EXIT
org.apache.commons.lang.mutable.MutableFloatTest.class$0 == orig(org.apache.commons.lang.mutable.MutableFloatTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testConstructors():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testConstructors():::EXIT
org.apache.commons.lang.mutable.MutableFloatTest.class$0 == orig(org.apache.commons.lang.mutable.MutableFloatTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testEquals():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testEquals():::EXIT
org.apache.commons.lang.mutable.MutableFloatTest.class$0 == orig(org.apache.commons.lang.mutable.MutableFloatTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testGetSet():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testGetSet():::EXIT
org.apache.commons.lang.mutable.MutableFloatTest.class$0 == orig(org.apache.commons.lang.mutable.MutableFloatTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testHashCode():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testHashCode():::EXIT
org.apache.commons.lang.mutable.MutableFloatTest.class$0 == orig(org.apache.commons.lang.mutable.MutableFloatTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testNanInfinite():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testNanInfinite():::EXIT
org.apache.commons.lang.mutable.MutableFloatTest.class$0 == orig(org.apache.commons.lang.mutable.MutableFloatTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testPrimitiveValues():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testPrimitiveValues():::EXIT
org.apache.commons.lang.mutable.MutableFloatTest.class$0 == orig(org.apache.commons.lang.mutable.MutableFloatTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testToString():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableFloatTest.testToString():::EXIT
org.apache.commons.lang.mutable.MutableFloatTest.class$0 == orig(org.apache.commons.lang.mutable.MutableFloatTest.class$0)
