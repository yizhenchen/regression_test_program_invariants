===========================================================================
org.apache.commons.lang.daikon.MutableObjectTest:::CLASS
===========================================================================
org.apache.commons.lang.daikon.MutableObjectTest.main(java.lang.String[]):::ENTER
org.apache.commons.lang.daikon.MutableObjectTest.class$0 == null
arg has only one value
arg.getClass().getName() == java.lang.String[].class
arg[] == []
arg[].toString == []
===========================================================================
org.apache.commons.lang.daikon.MutableObjectTest.main(java.lang.String[]):::EXIT
arg[] == orig(arg[])
org.apache.commons.lang.daikon.MutableObjectTest.class$0 has only one value
arg[] == []
arg[].toString == []
===========================================================================
org.apache.commons.lang.mutable.MutableObject:::CLASS
===========================================================================
org.apache.commons.lang.mutable.MutableObject:::OBJECT
===========================================================================
org.apache.commons.lang.mutable.MutableObject.MutableObject():::EXIT
this.value == null
===========================================================================
org.apache.commons.lang.mutable.MutableObject.MutableObject(java.lang.Object):::ENTER
===========================================================================
org.apache.commons.lang.mutable.MutableObject.MutableObject(java.lang.Object):::EXIT
this.value == orig(value)
this.value.getClass().getName() == orig(value.getClass().getName())
===========================================================================
org.apache.commons.lang.mutable.MutableObject.equals(java.lang.Object):::ENTER
this.value.getClass().getName() == java.lang.String.class
===========================================================================
org.apache.commons.lang.mutable.MutableObject.equals(java.lang.Object):::EXIT86
this.value has only one value
return == false
orig(obj.getClass().getName()) == org.apache.commons.lang.mutable.MutableObject.class
===========================================================================
org.apache.commons.lang.mutable.MutableObject.equals(java.lang.Object):::EXIT86;condition="not(return == true)"
===========================================================================
org.apache.commons.lang.mutable.MutableObject.equals(java.lang.Object):::EXIT87
return == true
orig(obj.getClass().getName()) == org.apache.commons.lang.mutable.MutableObject.class
===========================================================================
org.apache.commons.lang.mutable.MutableObject.equals(java.lang.Object):::EXIT87;condition="return == true"
===========================================================================
org.apache.commons.lang.mutable.MutableObject.equals(java.lang.Object):::EXIT88
this.value has only one value
return == false
orig(this) has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableObject.equals(java.lang.Object):::EXIT88;condition="not(return == true)"
===========================================================================
org.apache.commons.lang.mutable.MutableObject.equals(java.lang.Object):::EXIT
this.value == orig(this.value)
(return == false)  ==>  (this.value has only one value)
(return == true)  ==>  (orig(obj.getClass().getName()) == org.apache.commons.lang.mutable.MutableObject.class)
this.value.getClass().getName() == java.lang.String.class
this.value.getClass().getName() == orig(this.value.getClass().getName())
===========================================================================
org.apache.commons.lang.mutable.MutableObject.equals(java.lang.Object):::EXIT;condition="return == true"
return == true
orig(obj.getClass().getName()) == org.apache.commons.lang.mutable.MutableObject.class
===========================================================================
org.apache.commons.lang.mutable.MutableObject.equals(java.lang.Object):::EXIT;condition="not(return == true)"
this.value has only one value
return == false
===========================================================================
org.apache.commons.lang.mutable.MutableObject.getValue():::ENTER
===========================================================================
org.apache.commons.lang.mutable.MutableObject.getValue():::EXIT
this.value == return
return == orig(this.value)
this.value.getClass().getName() == return.getClass().getName()
return.getClass().getName() == orig(this.value.getClass().getName())
===========================================================================
org.apache.commons.lang.mutable.MutableObject.hashCode():::ENTER
this.value.getClass().getName() == java.lang.String.class
===========================================================================
org.apache.commons.lang.mutable.MutableObject.hashCode():::EXIT
this.value == orig(this.value)
this.value.getClass().getName() == java.lang.String.class
return one of { 0, 2035184, 62372158 }
org.apache.commons.lang.mutable.MutableObject.serialVersionUID > return
this.value.getClass().getName() == orig(this.value.getClass().getName())
===========================================================================
org.apache.commons.lang.mutable.MutableObject.setValue(java.lang.Object):::ENTER
this has only one value
this.value.getClass().getName() == java.lang.String.class
value.getClass().getName() == java.lang.String.class
===========================================================================
org.apache.commons.lang.mutable.MutableObject.setValue(java.lang.Object):::EXIT
this.value == orig(value)
this.value.getClass().getName() == java.lang.String.class
this.value.getClass().getName() == orig(value.getClass().getName())
===========================================================================
org.apache.commons.lang.mutable.MutableObject.toString():::ENTER
===========================================================================
org.apache.commons.lang.mutable.MutableObject.toString():::EXIT
this.value == orig(this.value)
return.toString one of { "10.0", "HI", "null" }
this.value.getClass().getName() == orig(this.value.getClass().getName())
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest:::CLASS
org.apache.commons.lang.mutable.MutableObjectTest.class$0 == null
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest:::OBJECT
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.MutableObjectTest(java.lang.String):::ENTER
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.MutableObjectTest(java.lang.String):::EXIT
testName.toString == orig(testName.toString)
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.testConstructors():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.testConstructors():::EXIT
org.apache.commons.lang.mutable.MutableObjectTest.class$0 == orig(org.apache.commons.lang.mutable.MutableObjectTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.testEquals():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.testEquals():::EXIT
org.apache.commons.lang.mutable.MutableObjectTest.class$0 == orig(org.apache.commons.lang.mutable.MutableObjectTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.testGetSet():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.testGetSet():::EXIT
org.apache.commons.lang.mutable.MutableObjectTest.class$0 == orig(org.apache.commons.lang.mutable.MutableObjectTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.testHashCode():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.testHashCode():::EXIT
org.apache.commons.lang.mutable.MutableObjectTest.class$0 == orig(org.apache.commons.lang.mutable.MutableObjectTest.class$0)
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.testToString():::ENTER
this has only one value
===========================================================================
org.apache.commons.lang.mutable.MutableObjectTest.testToString():::EXIT
org.apache.commons.lang.mutable.MutableObjectTest.class$0 == orig(org.apache.commons.lang.mutable.MutableObjectTest.class$0)
